# cat=Installation; type=boolean; label=LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_be.xlf:ext_conf.enableBootstrapPackageBackendLayouts
enableBootstrapPackageBackendLayouts = 0

# cat=Installation; type=boolean; label=LLL:EXT:t3_theme_diag/Resources/Private/Language/Backend.xlf:ext_conf.enableBootstrapPackageContainerElements
enableBootstrapPackageContainerElements = 0

# cat=Installation; type=boolean; label=LLL:EXT:t3_theme_diag/Resources/Private/Language/Backend.xlf:ext_conf.enableDefaultPageTSconfig
enableDefaultPageTSconfig = 1

# cat=Installation; type=boolean; label=LLL:EXT:t3_theme_diag/Resources/Private/Language/Backend.xlf:ext_conf.autoLoadStaticTSForExtensions
autoLoadStaticTSForExtensions = 1
