<?php

/**
 * Extension Manager/Repository config file for ext "t3_theme_diag".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'TYPO3 Theme: Diag',
    'description' => 'Diag is a very flexible TYPO3 theme built for Digital Marketing Agency, SEO Agency, Advertising Agency related businesses. You can easily create a modern website and start promoting your services in less than minutes. It is fully responsive and easy to Customize. Detailed documentation is included on how to install the theme, import demo content, and customize it.',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'extended_bootstrap_package' => '3.0.0-3.9.99',
            ],
        'conflicts' => [
        ],
        'suggests' => [
            'blog' => '11.0.0-11.0.99',
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'T3Themes\\T3ThemeDiag\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'T3graf Design-Team',
    'author_email' => 'support@t3graf-media.de',
    'author_company' => 'T3graf media-agentur UG',
    'version' => '1.0.0',
];
