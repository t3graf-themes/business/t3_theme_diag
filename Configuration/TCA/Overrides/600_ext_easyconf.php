<?php declare(strict_types=1);

/*
 * This file is part of the composer package buepro/typo3-pizpalue.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3') or die('Access denied.');

if (!ExtensionManagementUtility::isLoaded('easyconf') && !ExtensionManagementUtility::isLoaded('wstb_easyconf')) {
    $GLOBALS['TCA']['tx_easyconf_configuration']['ctrl'] = [
        'title' => '',
        'readOnly'  => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'enablecolumns' => [
        ],
        'searchFields' => '',
    ];
    return;
}

(function () {
    if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('wstb_easyconf') && \T3graf\WebsiteToolbox\Utility\ThemeHelper::isThemeActive('t3_theme_diag')) {
        //debug($GLOBALS['TCA']['tx_easyconf_configuration']['columns'], 'Altes TCA');
        $testArray = $GLOBALS['TCA']['tx_easyconf_configuration']['columns'];
        //$GLOBALS['TCA']['tx_easyconf_configuration']['columns'] = [];
        //debug($testArray, 'Zwischenspeicher Array');
        //debug($GLOBALS['TCA']['tx_easyconf_configuration']['columns'], 'Nach Löschung');
        //$GLOBALS['TCA']['tx_easyconf_configuration']['palettes'] = [];
        $tcaOverridesPathForPackage = ExtensionManagementUtility::extPath('t3_theme_diag')
            . 'Configuration/TCA/Easyconf';
        //debug($tcaOverridesPathForPackage); //die();
        if (!is_dir($tcaOverridesPathForPackage)) {
            return;
        }
        $files = scandir($tcaOverridesPathForPackage);
        if ($files === false) {
            return;
        }
        foreach ($files as $file) {
            if (is_file($tcaOverridesPathForPackage . '/' . $file)
                && ($file !== '.')
                && ($file !== '..')
                && (substr($file, -4, 4) === '.php')
            ) {
                require $tcaOverridesPathForPackage . '/' . $file;
            }
        }
        //debug($GLOBALS['TCA']['tx_easyconf_configuration']['columns'], 'Neues TCA');
        //debug(array_diff($testArray, $testArray), 'Vergleich');
        //die();
    }
})();
