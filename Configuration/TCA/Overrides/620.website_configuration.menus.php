<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('t3_theme_diag')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'main_menu_style' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:navigation_style',
                    'exclude' => 0,
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'select',
                        'renderType' => 'selectSingle',
                        'items' => [
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:navigation_style_light', 'default'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:navigation_style_light_transition', 'default-transition'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:navigation_style_dark', 'inverse'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:navigation_style_dark_transition', 'inverse-transition'],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.navigation.style',
                    ],
                ],
                'main_menu_type' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:navigation_type',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'select',
                        'renderType' => 'selectSingle',
                        'items' => [
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:navigation_type_default', ''],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:navigation_type_fixedTop', 'top'],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.navigation.type',
                    ],
                ],
                'breadcrumb_menu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:breadcrumb_menu',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.breadcrumb.enable',
                    ],
                ],
            ],

            'palettes' => [
                'main_nav' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:palette_main_navigation',
                    //'description' => 'LL: Header description',
                    'showitem' => 'main_menu_style, main_menu_type,',
                ],
                'available_menus' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf:available_menus',
                    //'description' => 'LL:available menus description',
                    'showitem' => 'breadcrumb_menu, meta_menu, language_menu,',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_menus.xlf')
        ->addDiv(
            'LANG:tabMenus',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        ->addPaletteToDiv('LANG:tabMenus', 'available_menus')
        ->addPaletteToDiv('LANG:tabMenus', 'main_nav')
        ->saveToTca(false);
}
