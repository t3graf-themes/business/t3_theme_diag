<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('t3_theme_diag')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'googleSiteVerification' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:googleSiteVerification',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.meta.google-site-verification',
                    ],
                ],
                'googleTagManagerContainerId' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:googleTagManagerContainerId',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.tracking.google.tagManagerContainerId',
                    ],
                ],
                'enableCookieConsent' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:enableCookieConsent',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxLabeledToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                                'labelChecked' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieConsent_activated',
                                'labelUnchecked' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieConsent_deactivated',
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.enable',
                    ],
                ],
                'cookieContentLink' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieContentLink',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'displayCond' => 'FIELD:enableCookieConsent:REQ:true',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    'blindLinkFields' => 'params',
                                    'blindLinkOptions' => 'file,folder,mail,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.content.href',
                    ],
                ],
                'cookieRegionalLaw' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieRegionalLaw',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'onChange' => 'reload',
                    'displayCond' => [
                        'AND' => [
                            'FIELD:enableCookieConsent:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.law.regionalLaw',
                    ],
                ],
                'cookieLocation' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieLocation',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:enableCookieConsent:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                            'FIELD:cookieRegionalLaw:REQ:false',
                        ],
                    ],
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.location',
                    ],
                ],
                'cookieCountryCode' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieCountryCode',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:enableCookieConsent:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                            'FIELD:cookieRegionalLaw:REQ:false',
                            'FIELD:cookieLocation:REQ:false',
                        ],
                    ],
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.law.countryCode',
                    ],
                ],
                'cookieLayout' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieLayout',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:enableCookieConsent:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'select',
                        'renderType' => 'selectSingle',
                        'items' => [
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieLayout.basic', 'basic'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieLayout.basic-close', 'basic-close'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieLayout.basic-header', 'basic-header'],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.layout',
                    ],
                ],
                'cookiePosition' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookiePosition',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:enableCookieConsent:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'select',
                        'renderType' => 'selectSingle',
                        'items' => [
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookiePosition.top', 'top'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookiePosition.bottom', 'bottom'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookiePosition.top-left', 'top-left'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookiePosition.top-right', 'top-right'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookiePosition.bottom-left', 'bottom-left'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookiePosition.bottom-right', 'bottom-right'],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.position',
                    ],
                ],
                'cookieType' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieType',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:enableCookieConsent:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'select',
                        'renderType' => 'selectSingle',
                        'items' => [
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieType.info', 'info'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieType.opt-in', 'opt-in'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieType.opt-out', 'opt-out'],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.type',
                    ],
                ],
                'cookieRevokable' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieRevokable',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:enableCookieConsent:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.revokable',
                    ],
                ],
                'cookieExpiryDays' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:cookieExpiryDays',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:enableCookieConsent:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'eval' => 'num',
                        'size' => 4,
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.cookieconsent.cookie.expiryDays',
                    ],
                ],
            ],

            'palettes' => [
                'google' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:pallette_google',
                    //'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:pallette_google_description',
                    'showitem' => 'googleSiteVerification, googleTagManagerContainerId',
                ],
                'cookieConsent' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:pallette_cookieConsent',
                    //'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf:pallette_cookieConsent_description',
                    'showitem' => 'enableCookieConsent, --linebreak--, cookieContentLink, --linebreak--, cookieRegionalLaw, cookieLocation, cookieCountryCode,--linebreak--, cookieLayout, cookiePosition, --linebreak--, cookieType, --linebreak--, cookieRevokable, --linebreak--, cookieExpiryDays',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_seogdpr.xlf')
        ->addDiv(
            'LANG:tabSeoGdpr',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        ->addPaletteToDiv('LANG:tabSeoGdpr', 'cookieConsent')
        ->addPaletteToDiv('LANG:tabSeoGdpr', 'google')
        ->saveToTca(false);
}
