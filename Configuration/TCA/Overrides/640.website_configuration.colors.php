<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('t3_theme_diag')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'color_primary' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:color_primary',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'colorpicker',
                        'size' => 10,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.primary',
                    ],
                ],
                'color_secondary' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:color_secondary',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'colorpicker',
                        'size' => 10,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.secondary',
                    ],
                ],
                'color_tertiary' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:color_tertiary',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'colorpicker',
                        'size' => 10,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.tertiary',
                    ],
                ],
                'color_quaternary' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:color_quaternary',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'colorpicker',
                        'size' => 10,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.quaternary',
                    ],
                ],
                'color_accent' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:color_accent',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                        'size' => 10,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.accent',
                    ],
                ],
                'color_light' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:color_light',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                        'size' => 10,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.light',
                    ],
                ],
                'color_dark' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:color_dark',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                        'size' => 10,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.dark',
                    ],
                ],
                'color_body-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:color_body_bg',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                        'size' => 10,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.body-bg',
                    ],
                ],
                'color_body-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:normal_text_color',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.body-color',
                    ],
                ],
                'color_headings-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:headings_color',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.headings-color',
                    ],
                ],
                'color_link-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:link_color',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.link-color',
                    ],
                ],
                'color_link-hover-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:link_hover_color',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.link-hover-color',
                    ],
                ],
                'color_topbar-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_background',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.topbar-bg',
                    ],
                ],
                'color_topbar-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.topbar-color',
                    ],
                ],
                'color_topbar-hover-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_hover_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.topbar-hover-color',
                    ],
                ],
                'color_topbar-bg-inverse' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_background',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.topbar-bg-inverse',
                    ],
                ],
                'color_topbar-inverse-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.topbar-inverse-color',
                    ],
                ],
                'color_topbar-inverse-hover-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_hover_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.topbar-inverse-hover-color',
                    ],
                ],
                'color_navbar-light-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_background',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-light-bg',
                    ],
                ],
                'color_navbar-light-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-light-color',
                    ],
                ],
                'color_navbar-light-hover-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_background_hover',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-light-hover-bg',
                    ],
                ],
                'color_navbar-light-hover-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_hover_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-light-hover-color',
                    ],
                ],
                'color_navbar-light-active-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_active_background',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-light-active-bg',
                    ],
                ],
                'color_navbar-light-active-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_active_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-light-active-color',
                    ],
                ],
                'color_navbar-light-disabled-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_disabled_background',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-light-disabled-bg',
                    ],
                ],
                'color_navbar-light-disabled-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_disabled_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:default',
                            'FIELD:main_menu_style:=:default-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-light-disabled-color',
                    ],
                ],
                'color_navbar-dark-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_background',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-dark-bg',
                    ],
                ],
                'color_navbar-dark-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-dark-color',
                    ],
                ],
                'color_navbar-dark-hover-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_background_hover',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-dark-hover-bg',
                    ],
                ],
                'color_navbar-dark-hover-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_hover_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-dark-hover-color',
                    ],
                ],
                'color_navbar-dark-active-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_active_background',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-dark-active-bg',
                    ],
                ],
                'color_navbar-dark-active-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_active_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-dark-active-color',
                    ],
                ],
                'color_navbar-dark-disabled-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_disabled_background',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-dark-disabled-bg',
                    ],
                ],
                'color_navbar-dark-disabled-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_disabled_text',
                    //'description' => 'LL:Descritpion',
                    'displayCond' => [
                        'OR' => [
                            'FIELD:main_menu_style:=:inverse',
                            'FIELD:main_menu_style:=:inverse-transition'
                        ]
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.navbar-dark-disabled-color',
                    ],
                ],
                'color_footer-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_background',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.footer-bg',
                    ],
                ],
                'color_footer-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_text',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.footer-color',
                    ],
                ],
                'color_footer-link-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:link_color',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.footer-link-color',
                    ],
                ],
                /*'color_footer-link-hover-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:link_hover_color',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.footer-link-hover-color',
                    ],
                ],*/
                'color_footer-meta-bg' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_background',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.footer-meta-bg',
                    ],
                ],
                'color_footer-meta-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:colors_text',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.footer-meta-color',
                    ],
                ],
                'color_footer-meta-link-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:link_color',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.footer-meta-link-color',
                    ],
                ],
               /* 'color_footer-meta-link-hover-color' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:link_hover_color',
                    //'description' => 'LL:Descritpion',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.footer-meta-link-hover-color',
                    ],
                ],
*/
            ],

            'palettes' => [
                'theme_colors' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_theme_colors',
                    //'description' => 'LL: Header description',
                    'showitem' => 'color_primary, color_secondary, color_tertiary, color_quaternary, --linebreak--, color_accent, color_light, color_dark, color_body-bg,',
                ],
                'text_colors' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_text_colors',
                    //'description' => 'LL: Header description',
                    'showitem' => 'color_body-color, color_headings-color, color_link-color, color_link-hover-color',
                ],
                'topbar_light_colors' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_topbar_light_colors',
                    //'description' => 'LL: Light topbar',
                    'showitem' => 'color_topbar-bg, color_topbar-color, color_topbar-hover-color,',
                ],
                'topbar_dark_colors' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_topbar_dark_colors',
                    //'description' => 'LL: Dark topbar',
                    'showitem' => 'color_topbar-bg-inverse, color_topbar-inverse-color, color_topbar-inverse-hover-color,',
                ],
                'navbar_light_colors' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_navbar_light_colors',
                    //'description' => 'LL: Header description',
                    'showitem' => 'color_navbar-light-bg, color_navbar-light-color, color_navbar-light-hover-bg, color_navbar-light-hover-color, --linebreak--, color_navbar-light-active-bg, color_navbar-light-active-color, color_navbar-light-disabled-bg, color_navbar-light-disabled-color,',
                ],
                'navbar_dark_colors' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_navbar_dark_colors',
                    //'description' => 'LL: Header description',
                    'showitem' => 'color_navbar-dark-bg, color_navbar-dark-color, color_navbar-dark-hover-bg, color_navbar-dark-hover-color, --linebreak--, color_navbar-dark-active-bg, color_navbar-dark-active-color, color_navbar-dark-disabled-bg, color_navbar-dark-disabled-color,',
                ],
                'footer_colors' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_footer_colors',
                    //'description' => 'LL: Header description',
                    'showitem' => 'color_footer-bg, color_footer-color, color_footer-link-color, color_footer-link-hover-color,',
                ],
                'footer_meta_colors' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf:palette_footer_meta_colors',
                    //'description' => 'LL: Header description',
                    'showitem' => 'color_footer-meta-bg, color_footer-meta-color, color_footer-meta-link-color, color_footer-meta-link-hover-color,',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf')
        ->addDiv(
            'LANG:tabColors',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        ->addPaletteToDiv('LANG:tabColors', 'footer_meta_colors')
        ->addPaletteToDiv('LANG:tabColors', 'footer_colors')
        ->addPaletteToDiv('LANG:tabColors', 'navbar_dark_colors')
        ->addPaletteToDiv('LANG:tabColors', 'navbar_light_colors')
        ->addPaletteToDiv('LANG:tabColors', 'topbar_dark_colors')
        ->addPaletteToDiv('LANG:tabColors', 'topbar_light_colors')
        ->addPaletteToDiv('LANG:tabColors', 'text_colors')
        ->addPaletteToDiv('LANG:tabColors', 'theme_colors')
        ->saveToTca(false);
}
