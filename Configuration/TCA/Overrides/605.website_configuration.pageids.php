<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('t3_theme_diag')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'pageBlog' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_pageBlog',
                    'exclude' => 0,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'int',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.tx_blog.settings.blogUid',
                    ],
                ],
                'pageBlogCategories' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_pageBlogCategories',
                    'exclude' => 0,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'int',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.tx_blog.settings.categoryUid',
                    ],
                ],
                'pageBlogAuthors' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_pageBlogAuthors',
                    'exclude' => 0,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'int',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.tx_blog.settings.authorUid',
                    ],
                ],
                'pageBlogTags' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_pageBlogTags',
                    'exclude' => 0,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'int',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.tx_blog.settings.tagUid',
                    ],
                ],
                'pageBlogArchive' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_pageBlogArchive',
                    'exclude' => 0,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'int',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.tx_blog.settings.archiveUid',
                    ],
                ],
                'pageBlogData' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_pageBlogData',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_pageBlogData_description',
                    'exclude' => 0,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.tx_blog.settings.storagePid',
                    ],
                ],
                'kesearchBlogSearchboxEnable' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_kesearchBlogSearchboxEnable',
                    'exclude' => 0,
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'theme.keSearch.blogEnable',
                    ],
                ],
                'kesearchBlogResultPage' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_kesearchBlogResultPage',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:kesearchBlogSearchboxEnable:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                        'eval' => 'int',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'theme.keSearch.blogSearchboxResultPage',
                    ],
                ],
                'kesearchBlogSearchboxID' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:field_kesearchBlogSearchboxID',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:kesearchBlogSearchboxEnable:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                        'eval' => 'int',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'theme.keSearch.blogSearchboxId',
                    ],
                ],
            ],
            'palettes' => [
                'kesearchBlog' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:palette_kesearchBlog',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:palette_kesearchBlog_description',
                    'showitem' => 'kesearchBlogSearchboxEnable, kesearchBlogResultPage, kesearchBlogSearchboxID,',
                ],
                'blog' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:palette_blog',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf:palette_blog_description',
                    'showitem' => 'pageBlog, pageBlogCategories, pageBlogAuthors, --linebreak--,pageBlogTags, pageBlogArchive, --linebreak--, pageBlogData,',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_pageids.xlf')
        ->addDiv(
            'LANG:tabPageids',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        ->addPaletteToDiv('LANG:tabPageids', 'blog')
        ->addPaletteToDiv('LANG:tabPageids', 'kesearchBlog')

        ->saveToTca(true);
}
