<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;


if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('t3_theme_diag')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'enable-rounded' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_style.xlf:enable_rounded',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.enable-rounded',
                    ],
                ],
                'enable-shadows' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_style.xlf:enable_shadows',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.enable-shadows',
                    ],
                ],
                'enable-gradients' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_style.xlf:enable_gradients',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.enable-gradients',
                    ],
                ],
                'enable_transitions' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_style.xlf:enable_transitions',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.enable-transitions',
                    ],
                ],
             /*  'customer_css' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_style.xlf:customer_css',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_style.xlf:customer_css_description',
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'text',
                        'renderType' => 't3editor',
                        'format' => 'css',
                        'cols' => 20,
                        'rows' => 10,
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.customer.css',
                    ],
                ],
                */
            ],

            'palettes' => [
                'style' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_style.xlf:palette_style',
                    //'description' => 'LL:description',
                    'showitem' => 'enable-rounded, enable-shadows, enable-gradients, enable_transitions, --linebreak--, customer_css',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_style.xlf')
        ->addDiv(
            'LANG:tabStyle',
            'before:--div--;LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:tabFonts'
        )
        ->addPaletteToDiv('LANG:tabStyle', 'style')
        ->saveToTca(false);
}

