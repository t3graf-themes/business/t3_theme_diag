<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('t3_theme_diag')) {

    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'socialmedia_menu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:socialmedia_enable',
                    //'description' => 'LL:Descritpion',
                    'exclude' => 0,
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.enable',
                    ],
                ],
                'facebook' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_facebook',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.facebook.url',
                    ],
                ],
                'twitter' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_twitter',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.twitter.url',
                    ],
                ],
                'instagram' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_instagram',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.instagram.url',
                    ],
                ],
                'github' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_github',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.github.url',
                    ],
                ],
                'googleplus' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_googleplus',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.googleplus.url',
                    ],
                ],
                'linkedin' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_linkedin',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.linkedin.url',
                    ],
                ],
                'xing' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_xing',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.xing.url',
                    ],
                ],
                'youtube' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_youtube',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.youtube.url',
                    ],
                ],
                'vk' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_vk',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.vk.url',
                    ],
                ],
                'vimeo' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_vimeo',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.vimeo.url',
                    ],
                ],
                'rss' => [
                    'exclude' => 1,
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:social_channel_rss',
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    //TODO make title available and save it
                                    'blindLinkFields' => 'class,params,target',
                                    'blindLinkOptions' => 'file,folder,mail,page,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.socialmedia.channels.rss.url',
                    ],
                ],

            ],

            'palettes' => [
                'sociallinks' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:pallette_social_channels',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf:pallette_social_channels_description',
                    'showitem' => 'facebook, twitter, --linebreak--, instagram, github, --linebreak--, googleplus, linkedin, --linebreak--, xing, youtube, --linebreak--, vk, vimeo, --linebreak--, rss',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_socialmedia.xlf')
        ->addDiv(
            'LANG:tabSocialMedia',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        ->addPaletteToDiv('LANG:tabSocialMedia', 'sociallinks')
        ->addFieldToDiv('LANG:tabSocialMedia', 'socialmedia_menu')
        ->saveToTca(false);

    }
