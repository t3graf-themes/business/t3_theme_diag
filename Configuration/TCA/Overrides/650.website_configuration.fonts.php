<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('t3_theme_diag')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // Available Google fonts
    $googleFonts = [
        ['Roboto', 'Roboto'],
        ['Open Sans', 'Open Sans'],
        ['Lato', 'Lato'],
        ['Monserrat', 'Monserrat'],
        ['Roboto Condensed', 'Roboto Condensed'],
        ['Source Sans Pro', 'Source Sans Pro'],
        ['Oswald', 'Oswald'],
        ['Raleway', 'Raleway'],
        ['PT Sans', 'PT Sans'],
        ['Noto Sans', 'Noto Sans'],
        ['Open Sans Condensed', 'Open Sans Condensed'],
        ['Ubuntu', 'Ubuntu'],
        ['Poppins', 'Poppins'],
        ['---', ''],
        ['Slabo 27px', 'Slabo 27px'],
        ['Merriweather', 'Merriweather'],
        ['Roboto Slab', 'Roboto Slab'],
        ['Noto Serif', 'Noto Serif'],
        ['Playfair Display', 'Playfair Display'],
        ['Bitter', 'Bitter'],
        ['Arvo', 'Arvo'],
        ['Libre Baskerville', 'Libre Baskerville'],
        ['---', ''],
        ['Lobster', 'Lobster'],
        ['Indie Flower', 'Indie Flower']
    ];
    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'google_font_enable' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:google_font_enable',
                    'exclude' => 1,
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.googleFont.enable',
                    ],
                ],
                'google_headings_font_enable' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:google_headings_font_enable',
                    'exclude' => 1,
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headingsFont.enable',
                    ],
                ],
                'font_size_root' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:font_size_root',
                    'exclude' => 1,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.font-size-root',
                    ],
                ],
                'google_font' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:google_font',
                    'displayCond' => 'FIELD:google_font_enable:REQ:true',
                    'exclude' => 0,
                    'config' => [
                        'eval' => 'required',
                        'type' => 'input',
                        'valuePicker' => [
                            'items' => $googleFonts
                        ],

                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.googleFont.font',
                    ],
                ],
                'google_font_weight' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:google_font_weight',
                    'displayCond' => 'FIELD:google_font_enable:REQ:true',
                    'exclude' => 0,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'required',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.googleFont.weight',
                    ],
                ],
                'google_headings_font' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:google_headings_font',
                    'displayCond' => [
                        'AND' => [
                            'FIELD:google_font_enable:REQ:true',
                            'FIELD:google_headings_font_enable:REQ:true'
                        ],
                    ],
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'required',
                        'valuePicker' => [
                            'items' => $googleFonts
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.google-headings-webfont',
                    ],
                ],
                'google_headings_font_weight' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:google_headings_font_weight',
                    'displayCond' => [
                        'AND' => [
                            'FIELD:google_font_enable:REQ:true',
                            'FIELD:google_headings_font_enable:REQ:true'
                        ],
                    ],
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'required',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.google-headings-webfont-weight',
                    ],
                ],
                'font_family' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:font_family',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.font-family-base',
                    ],
                ],
                'font_weight' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:font_weight',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'required',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.font-weight-normal',
                    ],
                ],
                'font_weight_light' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:font_weight_light',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.font-weight-light',
                    ],
                ],
                'font_weight_bold' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:font_weight_bold',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.font-weight-bold',
                    ],
                ],
                'font_size_base' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:font_size_base',
                    'exclude' => 1,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.font-size-base',
                    ],
                ],
                'headings_font_family' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_font_family',
                    'exclude' => 1,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:google_font_enable:REQ:true',
                            'FIELD:google_headings_font_enable:REQ:true'
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.headings-font-family',
                    ],
                ],
                'headings_font_weight' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_font_weight',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                        'eval' => 'required',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.headings-font-weight',
                    ],
                ],
                'headings_font_style' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_font_style',
                    'exclude' => 1,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.headings-font-style',
                    ],
                ],
                'headings_line_height' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_line_height',
                    'exclude' => 1,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.headings-line-height',
                    ],
                ],
                'headings_h1_font_size' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_h1_font_size',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h1-font-size',
                    ],
                ],
                'headings_h2_font_size' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_h2_font_size',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h2-font-size',
                    ],
                ],
                'headings_h3_font_size' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_h3_font_size',
                    'exclude' => 1,
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h3-font-size',
                    ],
                ],
                'headings_h4_font_size' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_h4_font_size',
                    'exclude' => 1,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h4-font-size',
                    ],
                ],
                'headings_h5_font_size' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_h5_font_size',
                    'exclude' => 1,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h5-font-size',
                    ],
                ],
                'headings_h6_font_size' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:headings_h6_font_size',
                    'exclude' => 1,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'plugin.bootstrap_package.settings.scss.h6-font-size',
                    ],
                ],
                'theme.googlefonts.url' => [
                    'label' => 'Google font Url',
                    'exclude' => 1,
                    'displayCond' => 'FIELD:showAllProperties:REQ:true',
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'theme.style.googleFontsUrl',
                    ],
                ],
            ],
            'palettes' => [
                'google_font_control' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:palette_google_fonts',
                    //'description' => 'LL: Header description',
                    'showitem' => 'google_font_enable, google_headings_font_enable, --linebreak--, font_size_root',
                ],
                'google_font' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:palette_google_font',
                    //'description' => 'LL: Header description',
                    'showitem' => 'google_font, google_font_weight, --linebreak--, google_headings_font, google_headings_font_weight',
                ],
                'font_normal' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:palette_google_font_normal',
                    //'description' => 'LL: Header description',
                    'showitem' => 'font_family, font_weight, --linebreak--, font_weight_light, font_weight_bold, --linebreak--, font_size_base',
                ],
                'font_headings' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf:palette_google_font_headings',
                    //'description' => 'LL: Header description',
                    'showitem' => 'headings_font_family, headings_font_weight,--linebreak--, headings_font_style, headings_line_height, --linebreak--, headings_h1_font_size, headings_h2_font_size, --linebreak--, headings_h3_font_size, headings_h4_font_size, --linebreak--, headings_h5_font_size, headings_h6_font_size',
                ],
            ],
        ],
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_fonts.xlf')
        ->addDiv(
            'LANG:tabFonts',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        ->addPaletteToDiv('LANG:tabFonts', 'font_headings')
        ->addPaletteToDiv('LANG:tabFonts', 'font_normal')
        ->addPaletteToDiv('LANG:tabFonts', 'google_font')
        ->addPaletteToDiv('LANG:tabFonts', 'google_font_control')
        ->saveToTca(false);
}
