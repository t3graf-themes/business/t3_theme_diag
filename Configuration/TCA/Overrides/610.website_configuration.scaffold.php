<?php

/*
 * This file is part of the package t3graf/website_toolbox.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TypesBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('website_toolbox') && \T3graf\WebsiteToolbox\Utility\TcaUtility::isThemeActive('t3_theme_diag')) {
    $typesBuilder = GeneralUtility::makeInstance(TypesBuilder::class);

    // add columns and palettes
    $GLOBALS['TCA']['tx_website_configuration'] = array_replace_recursive(
        $GLOBALS['TCA']['tx_website_configuration'],
        [
            'columns' => [
                'showTopbar' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:field_showTopbar',
                    'exclude' => 0,
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.enabled',
                    ],
                ],
                'showTopbarAddress' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_address',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showTopbar:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.address',
                    ],
                ],
                'showTopbarContact' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_contact',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showTopbar:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.contact',
                    ],
                ],
                'showTopbarSocialLinks' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_social_links',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showTopbar:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.sociallinks',
                    ],
                ],
                'showTopbarLanguageMenu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_language_menu',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showTopbar:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.languageMenu',
                    ],
                ],
                'showTopbarLogin' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_login',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showTopbar:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.headertopinfo.items.loginLink.enable',
                    ],
                ],
                'showFooter' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_footer',
                    'exclude' => 0,
                    'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footer.enable',
                    ],
                ],
                'footerFullWith' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_full_width',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footer.fullwidth',
                    ],
                ],
                'footerSlideOut' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_slide_out',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footer.slideout',
                    ],
                ],
                'footerMenu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_footer_menu',
                    'onChange' => 'reload',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footernavigation.enable',
                    ],
                ],
                'footerMenuNavigationValue' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_menu_navigation_value',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_menu_navigation_description',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerMenu:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footernavigation.navigationValue',
                    ],
                ],
                'footerMenuType' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_menu_type',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerMenu:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'select',
                        'renderType' => 'selectSingle',
                        'items' => [
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_menu_type_list', 'list'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_menu_type_directory', 'directory'],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footernavigation.navigationType',
                    ],
                ],
                'footerMenuShowNotInMenu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_menu_not_in_menu',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerMenu:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footernavigation.includeNotInMenu',
                    ],
                ],
                'footerMenuEnableIcon' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_menu_enable_icon',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerMenu:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.footernavigation.icon.enable',
                    ],
                ],
                'footerMetaMenu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_footer_meta_menu',
                    'onChange' => 'reload',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.meta.enable',
                    ],
                ],
                'footerContact' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footerContact',
                    'onChange' => 'reload',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.contact.enable',
                    ],
                ],
                'footerContactLabel' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footerContactLabel',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerContact:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.contact.label',
                    ],
                ],
                'footerContactButtonClass' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footerContactButtonClass',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerContact:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.contact.button.colorClass',
                    ],
                ],
                'footerContactButtonLabel' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footerContactButtonLabel',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerContact:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.contact.button.label',
                    ],
                ],
                'footerContactButtonPageUid' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footerContactButtonPageUid',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerContact:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                        'renderType' => 'inputLink',
                        'fieldControl' => [
                            'linkPopup' => [
                                'options' => [
                                    'blindLinkFields' => 'class,params,target,title',
                                    'blindLinkOptions' => 'file,folder,mail,url,telephone',
                                ],
                            ],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.contact.button.pageUid',
                    ],
                ],
                'footerMetaMenuNavigationValue' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_meta_menu_navigation_value',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_meta_menu_navigation_description',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerMetaMenu:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'input',
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.meta.navigationValue',
                    ],
                ],
                'footerMetaMenuType' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_meta_menu_type',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerMetaMenu:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'select',
                        'renderType' => 'selectSingle',
                        'items' => [
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_meta_menu_type_list', 'list'],
                            ['LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_meta_menu_type_directory', 'directory'],
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.meta.navigationType',
                    ],
                ],
                'footerMetaMenuShowNotInMenu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:footer_meta_menu_not_in_menu',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:footerMetaMenu:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    //'onChange' => 'reload',
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.meta.includeNotInMenu',
                    ],
                ],
                'footerLanguageMenu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_language_menu',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.language.enable',
                    ],
                ],
                'showCopyright' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:show_copyright',
                    'exclude' => 0,
                    'displayCond' => [
                        'AND' => [
                            'FIELD:showFooter:REQ:true',
                            'FIELD:showAllProperties:REQ:true',
                        ],
                    ],
                    'config' => [
                        'type' => 'check',
                        'renderType' => 'checkboxToggle',
                        'items' => [
                            [
                                0 => '',
                                1 => '',
                            ]
                        ],
                    ],
                    'website_configuration_field_mapper' => [
                        'mapper' => TypoScriptConstantMapper::class,
                        'path' => 'page.theme.copyright.enable',
                    ],
                ],
            ],
            'palettes' => [
                'topbar' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_topbar',
                    'showitem' => 'showTopbar,',
                ],
                'topbarElements' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_topbar_elements',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_topbar_elements_description',
                    'showitem' => 'showTopbarAddress, showTopbarContact, showTopbarSocialLinks, showTopbarLanguageMenu, --linebreak--, showTopbarLogin,',
                ],
                'footer' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footer',
                    'showitem' => 'showFooter, footerFullWith, footerSlideOut,',
                ],
                'footerElements' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footerElements',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footer_elements_description',
                    'showitem' => 'footerMenu, footerMetaMenu, footerContact, footerLanguageMenu, --linebreak--, showCopyright',
                ],
                'footerMenu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footerMenu',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footer_elements_description',
                    'showitem' => 'footerMenuType, footerMenuShowNotInMenu, footerMenuEnableIcon, --linebreak--, footerMenuNavigationValue',
                ],
                'footerMetaMenu' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footerMetaMenu',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footer_elements_description',
                    'showitem' => 'footerMetaMenuType, footerMetaMenuShowNotInMenu, --linebreak--, footerMetaMenuNavigationValue',
                ],
                'footerContact' => [
                    'label' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_Contact',
                    'description' => 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf:palette_footer_contact_description',
                    'showitem' => 'footerContactLabel, footerContactButtonLabel, --linebreak--, footerContactButtonPageUid, footerContactButtonClass',
                ],
            ],
        ]
    );

    // build TCA types
    $typesBuilder
        ->loadConfiguration()
        ->useLocalLangFile('EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_scaffold.xlf')
        ->addDiv(
            'LANG:tabScaffold',
            'before:--div--;LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:tabMaintenance'
        )
        ->addPaletteToDiv('LANG:tabScaffold', 'footerMetaMenu')
        ->addPaletteToDiv('LANG:tabScaffold', 'footerContact')
        ->addPaletteToDiv('LANG:tabScaffold', 'footerMenu')
        ->addPaletteToDiv('LANG:tabScaffold', 'footerElements')
        ->addPaletteToDiv('LANG:tabScaffold', 'footer')
        ->addPaletteToDiv('LANG:tabScaffold', 'topbarElements')
        ->addPaletteToDiv('LANG:tabScaffold', 'topbar')

        ->saveToTca(true);
}
