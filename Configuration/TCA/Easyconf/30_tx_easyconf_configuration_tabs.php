<?php declare(strict_types=1);

/*
 * This file is part of the composer package buepro/typo3-pizpalue.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

use Buepro\Easyconf\Utility\TcaUtility;

defined('TYPO3') or die('Access denied.');

(static function () {
    $l10nFile = 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_easyconf_tabs.xlf';
    $tca = &$GLOBALS['TCA']['tx_easyconf_configuration'];

    /**
     * Define type
     */
    $tabs = [
        'tabWebsite' => implode(', ', [
            '--palette--;;paletteConfiguration',
            '--palette--;;paletteOwner',
            '--palette--;;paletteOwner_address',
            '--palette--;;paletteOwner_communication',
        ]),
        /*
        'tabScaffold' => implode(', ', [
            sprintf('--palette--;%s:paletteScaffoldElements;paletteScaffoldElements', $l10nFile),
            sprintf('--palette--;%s:paletteScaffoldHeader;paletteScaffoldHeader', $l10nFile),
            sprintf('--palette--;%s:paletteScaffoldFooter;paletteScaffoldFooter', $l10nFile),
            sprintf('--palette--;%s:paletteScaffoldCopyright;paletteScaffoldCopyright', $l10nFile),
            sprintf('--palette--;%s:paletteScaffoldContact;paletteScaffoldContact', $l10nFile),
        ]),
        'tabMenu' => implode(', ', [
            sprintf('--palette--;%s:paletteMenuSelect;paletteMenuSelect', $l10nFile),
            sprintf('--palette--;%s:paletteMenuMain;paletteMenuMain', $l10nFile),
            sprintf('--palette--;%s:paletteMenuMainSubpage;paletteMenuMainSubpage', $l10nFile),
            sprintf('--palette--;%s:paletteMenuToggler;paletteMenuToggler', $l10nFile),
            sprintf('--palette--;%s:paletteMenuFast;paletteMenuFast', $l10nFile),
            sprintf('--palette--;%s:paletteMenuFooter;paletteMenuFooter', $l10nFile),
            sprintf('--palette--;%s:paletteMenuCopyright;paletteMenuCopyright', $l10nFile),
            sprintf('--palette--;%s:paletteMenuLanguage;paletteMenuLanguage', $l10nFile),
            sprintf('--palette--;%s:paletteMenuMeta;paletteMenuMeta', $l10nFile),
            sprintf('--palette--;%s:paletteMenuScroll;paletteMenuScroll', $l10nFile),
        ]),
        'tabLogo' => implode(', ', [
            sprintf('--palette--;%s:paletteLogo;paletteLogo', $l10nFile),
            sprintf('--palette--;%s:paletteAppicon;paletteAppicon', $l10nFile),
        ]),
        */
        'tabColors' => implode(', ', [
            sprintf('--palette--;%s:paletteColorsMain;paletteColorsMain', 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf'),
            sprintf('--palette--;%s:paletteColorsText;paletteColorsText', 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf'),
            sprintf('--palette--;%s:paletteTopbarLightColors;paletteTopbarLightColors', 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf'),
            sprintf('--palette--;%s:paletteTopbarDarkColors;paletteTopbarDarkColors', 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf'),
            sprintf('--palette--;%s:paletteColorsLightHeader;paletteColorsLightHeader', 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf'),
            sprintf('--palette--;%s:paletteColorsDarkHeader;paletteColorsDarkHeader', 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf'),
            sprintf('--palette--;%s:paletteColorsFooter;paletteColorsFooter', 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf'),
            sprintf('--palette--;%s:paletteColorsFooterMeta;paletteColorsFooterMeta', 'LLL:EXT:t3_theme_diag/Resources/Private/Language/locallang_tab_colors.xlf'),
        ]),
            /*
        'tabFont' => implode(', ', [
            sprintf('--palette--;%s:paletteFontControl;paletteFontControl', $l10nFile),
            sprintf('--palette--;%s:paletteFontGeneral;paletteFontGeneral', $l10nFile),
            sprintf('--palette--;%s:paletteFontGoogle;paletteFontGoogle', $l10nFile),
            sprintf('--palette--;%s:paletteFontNormal;paletteFontNormal', $l10nFile),
            sprintf('--palette--;%s:paletteFontHeadings;paletteFontHeadings', $l10nFile),
        ]),
        'tabSocial' => implode(', ', [
            sprintf('--palette--;%s:paletteSocialControl;paletteSocialControl', $l10nFile),
            sprintf('--palette--;%s:paletteSocialChannel;paletteSocialChannel', $l10nFile),
        ]),
        'tabStyle' => implode(', ', [
            sprintf('--palette--;%s:paletteStyleGlobal;paletteStyleGlobal', $l10nFile),
            sprintf('--palette--;%s:paletteStyleRounded;paletteStyleRounded', $l10nFile),
            sprintf('--palette--;%s:paletteStyleScss;paletteStyleScss', $l10nFile),
            sprintf('--palette--;%s:paletteStyleVarious;paletteStyleVarious', $l10nFile),
        ]),
        'tabFeature' => implode(', ', [
            sprintf('--palette--;%s:paletteFeatureControl;paletteFeatureControl', $l10nFile),
            sprintf('--palette--;%s:paletteFeatureVarious;paletteFeatureVarious', $l10nFile),
        ]),
        */
        'tabMaintenance' => implode(', ', [
            sprintf('--palette--;%s:paletteMaintenance;paletteMaintenance', $l10nFile),
            sprintf('--palette--;%s:paletteSeo;paletteSeo', $l10nFile),
            sprintf('--palette--;%s:paletteGoogle;paletteGoogle', $l10nFile),
            sprintf('--palette--;%s:paletteCookie;paletteCookie', $l10nFile),
        ]),
        'tabAdmin' => implode(', ', [
            sprintf('showAllProperties, --linebreak--, admPanel,%s:paletteAdmin_settings', $l10nFile),
            //sprintf('--palette--;%s:paletteAdmin_settings;paletteAdmin_settings', $l10nFile),
            sprintf('--palette--;%s:paletteAgency;paletteAgency', $l10nFile),
            //sprintf('--palette--;%s:paletteAdminSiteconf;paletteAdminSiteconf', $l10nFile),
            //sprintf('--palette--;%s:paletteAdminFormat;paletteAdminFormat', $l10nFile),
        ]),

    ];
    //debug($tabs); die();
    $tca['types'][0] = TcaUtility::getType($tabs, $l10nFile);

    unset($tca);
})();
