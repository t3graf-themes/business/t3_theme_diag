<?php

use Buepro\Easyconf\Mapper\SiteConfigurationMapper;

defined('TYPO3_MODE') || die();

// Register fields

$GLOBALS['TCA']['tx_easyconf_configuration']['columns'] = array_replace_recursive(
    $GLOBALS['TCA']['tx_easyconf_configuration']['columns'],
    [
        'websiteTitle' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:websiteTitle',
            'displayCond' => 'FIELD:showAllProperties:=:1',
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'websiteTitle',
            ],
        ],
        'websiteCopyright' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:websiteCopyright',
            'displayCond' => 'FIELD:showAllProperties:=:1',
            'config' => [
                'type' => 'text',
                'renderType' => 't3editor',
                'format' => 'html',
                'rows' => 2,
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.copyright',
            ],
        ],
        'company' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_company',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.company',
            ],
        ],
        'contact_name' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_name',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_name',
            ],
        ],
        'domain' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_domain',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.domain',
            ],
        ],
        'contact_address' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_address',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_address',
            ],
        ],
        'contact_address_supplement' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_address_supplement',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_address_supplement',
            ],
        ],
        'contact_zip' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_zip',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_zip',
            ],
        ],
        'contact_city' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_city',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_city',
            ],
        ],
        'contact_country' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_country',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_country',
            ],
        ],
        'contact_phone' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_phone',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_phone',
            ],
        ],
        'contact_fax' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_fax',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_fax',
            ],
        ],
        'contact_email' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_email',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'input',
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_email',
            ],
        ],
        'contact_office_hours' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:owner_office_hours',
            'displayCond' => [
                'AND' => [
                    // 'HIDE_FOR_NON_ADMINS',
                    'FIELD:showAllProperties:REQ:true',
                ],
            ],
            'config' => [
                'type' => 'text',
                'cols' => 20,
                'rows' => 2,
            ],
            'tx_easyconf' => [
                'mapper' => SiteConfigurationMapper::class,
                'path' => 'settings.sitepackage.owner.contact_office_hours',
            ],
        ],
    ]
);

$GLOBALS['TCA']['tx_easyconf_configuration']['palettes'] = array_replace_recursive(
    $GLOBALS['TCA']['tx_easyconf_configuration']['palettes'],
    [
        'paletteConfiguration' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:palette_configuration',
            //'description' => 'LL: Configuration description',
            'showitem' => 'websiteTitle, --linebreak--, websiteCopyright',
        ],
        'paletteOwner' => [
            'label' => 'LLL:EXT:website_toolbox/Resources/Private/Language/locallang_configuration_form.xml:palette_owner',
            'showitem' => 'company, contact_name, --linebreak--, domain,',
        ],
        'paletteOwner_address' => [
            'label' => '',
            'showitem' => 'contact_address, contact_address_supplement, --linebreak--, contact_zip, contact_city, --linebreak--, contact_country,',
        ],
        'paletteOwner_communication' => [
            'label' => '',
            'showitem' => 'contact_phone, contact_fax, --linebreak--, contact_email, contact_office_hours,',
        ],
    ]
);

