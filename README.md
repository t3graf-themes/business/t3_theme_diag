TYPO3 Theme: Diag - SEO und Digital Marketing Agency Theme
==============================================================

Diag is a very flexible TYPO3 theme built for Digital Marketing Agency, SEO Agency, Advertising Agency related businesses. You can easily create a modern website and start promoting your services in less than minutes. It is fully responsive and easy to Customize. Detailed documentation is included on how to install the theme, import demo content, and customize it.

## 1. Features

## Dependency:

- TYPO3:
- [bk2k/bootstrap-package](https://extensions.typo3.org/extension/bootstrap_package): ^12
- t3graf/extended-bootstrap-packages


## Supported extensions
-   [ke_search](https://extensions.typo3.org/extension/ke_search)
- [t3g/blog](https://extensions.typo3.org/extension/blog): ^11

---
Developed 2021 by T3graf media-agentur UG

If you have any questions about this project or just want to talk:
Send me a message [support@t3graf-media.de](mailto:support@t3graf-media.de)
