# 1.0.0

## FEATURE

- 0e236b8 [FEATURE] make theme compatible with EXT:website_toolbox from T3graf media-agentur
- 766c38e [FEATURE] add more special icons for theme
- 2fc4512 [FEATURE] add more special icons for theme
- a0a3da2 [FEATURE] add special icons for theme
- 344ea3d [FEATURE] add sliding for after content in BlogPage
- 3af4378 [FEATURE] add ke_search templates for search function in blog
- 79a22ec [FEATURE] add Social Links to blog
- 271225c [FEATURE] add EXT:blog.
- 599013b [FEATURE] add scss for navbar.
- 70faed0 [FEATURE] add some css classes to headline.

## TASK

- aee3139 [TASK] update composer version
- f80b79f [TASK] add EXT:blog as suggestion
- a0a5d07 [TASK] update bootstrap package to v12
- 1ca0e56 [TASK] add icon provider for own theme icons.
- 026668b [TASK] update required extensions
- b737d7a [TASK] cleanup BlogPage.html
- 110ffcb [TASK] revert font-size-base
- 82f93d5 [TASK] change font-size h3 in texticon-content
- 798db7c [TASK] change font-size-base
- e0324c7 [TASK] wrap post content with  tag
- 1f378fd [TASK] change colors of topinfobar
- 3411338 [TASK] change design of blog social buttons
- e1a70cc [TASK] implement blog template
- a4d7a16 [TASK] change carousel-item .h2 and .h4
- f60c211 [TASK] delete example backendlayout
- ff7fae7 [TASK] change font EXO2
- 91ab6a0 [TASK] change font EXO2
- ea8e981 [TASK] change look of social media CE
- a4dda3a [TASK] add new menuPages class
- 15ffd79 [TASK] add footer-headline class
- 231d130 [TASK] add more headline classes with colors
- 5913fdc [TASK] update theme color profile
- d2c2709 [TASK] update theme color profile
- baa4e98 [TASK] update extended_bootstrap_package
- bbcf4bb [TASK] update bootstrap_package
- d90263b [TASK] update extended_bootstrap_package
- 1d16588 [TASK] swap primary and secondary color
- c3e0a2b [TASK] implement EXT:blog in configuration
- cf75070 [TASK] extend README.md
- cccca51 [TASK] change text decoration for links.
- 06b5747 [TASK] change VendorName to T3Themes

## BUGFIX

- 013c350 [BUGFIX] fix suggestion for EXT:blog in TypoScript
- 5a80ad9 [BUGFIX] fix namespace error for icon provider
- 90971d5 [BUGFIX] fix target in rss feed link
- f61ac4a [BUGFIX] fix ke_search css loading
- a7929f0 [BUGFIX] constants names of infotopbar colors
- 3c64116 [BUGFIX] change colors of topinfobar
- eaaf2de [BUGFIX] change infobar color
- 8fe549a [BUGFIX] fix tertiary headline color
- a09421c [BUGFIX] fix problem with EXT:extended_bootstrap_package css implementation

## MISC

- e360cfe Initial commit

## Contributors

- Mike Tölle

