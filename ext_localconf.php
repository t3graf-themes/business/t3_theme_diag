<?php
defined('TYPO3_MODE') || die();

// Initialization
$extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
);
$bootstrapPackageConfiguration = $extensionConfiguration->get('bootstrap_package');
$diagConfiguration = $extensionConfiguration->get('t3_theme_diag');

// Add BackendLayouts for the BackendLayout DataProvider
if (!(bool) $bootstrapPackageConfiguration['disablePageTsBackendLayouts']) {
    // Disable some bootstrap_package backend layouts
    if (!(bool) $diagConfiguration['enableBootstrapPackageBackendLayouts']) {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            "@import 'EXT:t3_theme_diag/Configuration/TsConfig/Page/Mod/WebLayout/DisableBackendLayouts.tsconfig'"
        );
    }
    // Theme backend layouts
   \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
       "@import 'EXT:t3_theme_diag/Configuration/TsConfig/Page/Mod/WebLayout/BackendLayouts/*.tsconfig'"
   );
}

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['t3_theme_diag'] = 'EXT:t3_theme_diag/Configuration/RTE/Default.yaml';

/***************
 * PageTS
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:t3_theme_diag/Configuration/TsConfig/Page/All.tsconfig">');

// Register icon provider
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/bootstrap-package/icons']['provider'][\T3Themes\T3ThemeDiag\Icons\ThemeiconsProvider::class]
    = \T3Themes\T3ThemeDiag\Icons\ThemeiconsProvider::class;
