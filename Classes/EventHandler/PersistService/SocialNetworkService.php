<?php

declare(strict_types=1);

/*
 * This file is part of the composer package buepro/typo3-pizpalue.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3Themes\T3ThemeDiag\EventHandler\PersistService;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Service\TypoLinkCodecService;

class SocialNetworkService extends AbstractService
{
    /** @var string[]  */
    public static $socialChannels = ['facebook', 'github', 'googleplus', 'instagram', 'linkedin', 'pinterest',
        'rss', 'twitter', 'vimeo', 'vk', 'xing', 'youtube'];

    public function process(): void
    {
        $typoLinkCodecService = GeneralUtility::makeInstance(TypoLinkCodecService::class);
        foreach (self::$socialChannels as $channel) {
            $path = 'page.theme.socialmedia.channels.' . $channel . '.';
            $link = $this->formFields[$channel] ?? '';
            $decoded = $typoLinkCodecService->decode($link);
            if ($this->typoScriptMapper->getInheritedFileConstantsProperty($path . 'label') !== $decoded['title']) {
                $this->typoScriptMapper->bufferProperty(
                    $path . 'label',
                    $decoded['title'] ?? ''
                );
            }
            if ($this->typoScriptMapper->getInheritedFileConstantsProperty($path . 'url') !== $decoded['url']) {
                $this->typoScriptMapper->bufferProperty(
                    $path . 'url',
                    $decoded['url'] ?? ''
                );
            }
        }
    }
}
