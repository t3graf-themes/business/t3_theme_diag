<?php

declare(strict_types=1);

/*
 * This file is part of the composer package buepro/typo3-pizpalue.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3Themes\T3ThemeDiag\EventHandler;

use T3graf\WebsiteToolbox\Event\AfterReadingPropertiesEvent;
use T3Themes\T3ThemeDiag\EventHandler\ReadService\FontService;
use T3Themes\T3ThemeDiag\EventHandler\ReadService\ScssService;
use T3Themes\T3ThemeDiag\EventHandler\ReadService\SocialNetworkService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class AfterReadingPropertiesEventHandler
{
    /** @var array */
    protected $formFields = [];

    public function __invoke(AfterReadingPropertiesEvent $event): void
    {
        $formFields = $event->getFormFields();
        $classes = [SocialNetworkService::class, FontService::class];
        foreach ($classes as $class) {
            $formFields = GeneralUtility::makeInstance($class, $formFields)->process();
        }
        $event->setFormFields($formFields);
    }
}
