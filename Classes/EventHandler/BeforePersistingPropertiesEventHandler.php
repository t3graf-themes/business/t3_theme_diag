<?php

declare(strict_types=1);

/*
 * This file is part of the composer package buepro/typo3-pizpalue.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3Themes\T3ThemeDiag\EventHandler;

use T3graf\WebsiteToolbox\Event\BeforePersistingPropertiesEvent;
use T3graf\WebsiteToolbox\Mapper\TypoScriptConstantMapper;
use T3graf\WebsiteToolbox\Utility\TcaUtility;
use T3Themes\T3ThemeDiag\EventHandler\PersistService\FontService;
use T3Themes\T3ThemeDiag\EventHandler\PersistService\ScssService;
use T3Themes\T3ThemeDiag\EventHandler\PersistService\SocialNetworkService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class BeforePersistingPropertiesEventHandler
{
    public function __invoke(BeforePersistingPropertiesEvent $event): void
    {
        $serviceClassNames = [SocialNetworkService::class, FontService::class];
        $formFields = $event->getFormFields();
        $configurationRecord = $event->getConfigurationRecord();
        foreach ($serviceClassNames as $serviceClassName) {
            GeneralUtility::makeInstance(
                $serviceClassName,
                $formFields,
                $configurationRecord
            )->process();
        }
        foreach ($formFields as $field => $value) {
            if (
                $value === '??' &&
                ($class = TcaUtility::getMappingClass($field)) !== null &&
                $class = TypoScriptConstantMapper::class
            ) {
                GeneralUtility::makeInstance($class)->removePropertyFromBuffer(TcaUtility::getMappingPath($field));
            }
        }
    }
}
