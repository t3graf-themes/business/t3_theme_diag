<?php
namespace T3Themes\T3ThemeDiag\ExpressionLanguage;

use TYPO3\CMS\Core\ExpressionLanguage\AbstractProvider;

class ThemeConditionProvider extends AbstractProvider
{
    public function __construct()
    {
        $this->expressionLanguageProviders = [
            ThemeConditionFunctionsProvider::class,
        ];
    }
}
